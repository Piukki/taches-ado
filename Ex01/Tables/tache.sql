﻿CREATE TABLE [dbo].[tache]
(
  [id] int PRIMARY KEY,
  [IdPersonne] int NOT NULL,
  [IdCategorie]int NOT NULL,
  [nom] VARCHAR(50) NOT NULL,
  [descript] VARCHAR(256) NOT NULL,
  [dateDeCreation] DATETIME2 NOT NULL,
  [datePredicFin] DATETIME2 NOT NULL, 
  [dateFinReelle] DATETIME2 NULL, 
  CONSTRAINT [FK_tache_personne] FOREIGN KEY ([IdPersonne]) REFERENCES [personne]([IdPersonne]), 
  CONSTRAINT [FK_tache_categorie] FOREIGN KEY ([IdCategorie]) REFERENCES [categorie]([IdCategorie]),
  CONSTRAINT [CK_dateFinReelle] CHECK (dateFinReelle > dateDeCreation)
)
