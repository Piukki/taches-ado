﻿CREATE TABLE [dbo].[personne]
(
  [IdPersonne] INT PRIMARY KEY NOT NULL,
  [nom] VARCHAR(50) NOT NULL,
  [prenom] VARCHAR(50) NOT NULL
)
