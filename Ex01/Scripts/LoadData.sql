﻿/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

USE Ex01

INSERT INTO categorie (IdCategorie, nom) VALUES 
(1, 'Manger'),
(2, 'Lessive'),
(3, 'Ménage'),
(4, 'Coder du Javascript'),
(5, 'Bricoler'),
(6, 'Jardiner');

INSERT INTO personne (IdPersonne, nom, prenom) VALUES
(1, 'Levis', 'Quentin'),
(2, 'Lina', 'Nathalie'),
(3, 'Rey', 'Piukki'),
(4, 'DK', 'Amaranth'),
(5, 'Gelpi', 'Lyia'),
(6, 'Dubray', 'Benjamin');

INSERT INTO tache (Id, IdPersonne ,IdCategorie, nom, descript, dateDeCreation, datePredicFin) VALUES 
(1, 6, 4, 'Tâche 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam massa lacus.', CAST(GETDATE() as datetime2), '2044-06-27 14:40:00'),
(2, 2, 1, 'Tâche 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam massa lacus.', '2022-06-27 18:40:00', '2022-06-27 19:20:00'),
(3, 1, 6, 'Tâche 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam massa lacus.', CAST(GETDATE() as datetime2), '2022-06-27 16:00:00'),
(4, 3, 2, 'Tâche 4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam massa lacus.', CAST(GETDATE() as datetime2), '2022-07-04 14:52:00'),
(5, 5, 3, 'Tâche 5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam massa lacus.', CAST(GETDATE() as datetime2), '2022-06-30 10:00:00'),
(6, 4, 5, 'Tâche 6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam massa lacus.', CAST(GETDATE() as datetime2), '2022-08-17 00:00:00');
