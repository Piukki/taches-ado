﻿CREATE TABLE [dbo].[Student]
(
    [Id] INT NOT NULL IDENTITY,
    [FirstName] VARCHAR(50) NOT NULL,
    [LastName] VARCHAR(50) NOT NULL,
    [BirthDate] DATETIME2 NOT NULL,
    [YearResult] INT NOT NULL,
    [SectionId] INT NOT NULL,
    [Active] BIT NOT NULL CONSTRAINT DK_Student_Active DEFAULT (1),
    CONSTRAINT [CK_Student_YearResult] CHECK (YearResult BETWEEN 0 AND 20),
    CONSTRAINT [CK_Studen_BirthDate] CHECK (BirthDate <= '2004-01-01'),
    CONSTRAINT [PK_Student] Primary KEY ([Id]), 
    CONSTRAINT [FK_Student_Section] FOREIGN KEY ([SectionId]) REFERENCES [Section]([Id]),
)