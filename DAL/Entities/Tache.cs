﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Tache
    {
        public int id { get; set; }
        public int IdPersonne { get; set; }
        public int IdCategorie { get; set; }
        public string nom { get; set; }
        public string descript { get; set; }
        public DateTime dateDeCreation { get; set; }
        public DateTime datePredicFin { get; set; }
        public DateTime? dateFinReelle { get; set; }
    }
}
