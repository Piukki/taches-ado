﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Personne
    {
        public int IdPersonne { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
    }
}
