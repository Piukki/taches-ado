﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class TacheService
    {
        public string ConnectionStringSSMS
        {
            get { return "server=MYA-PC\\SQLEXPRESS;database=Ex01;integrated security=true"; }
        }

        public IEnumerable<Tache> GetAll()
        {
            List<Tache> tache = new List<Tache>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM tache";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Tache tacheToSee = new Tache()
                    {
                        id = (int)reader["id"],
                        IdPersonne = (int)reader["IdPersonne"],
                        IdCategorie = (int)reader["IdCategorie"],
                        nom = (string)reader["nom"],
                        descript = (string)reader["descript"],
                        dateDeCreation = (DateTime)reader["dateDeCreation"],
                        datePredicFin = (DateTime)reader["datePredicFin"],
                        dateFinReelle = reader["dateFinReelle"] != DBNull.Value? (DateTime)reader["dateFinReelle"]:null
                    };
                    tache.Add(tacheToSee);
                }
                connection.Close();
            }
            return tache;
        }
        public Tache GetById(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM tache WHERE Id = @id";
                command.Parameters.AddWithValue("id", id);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return new Tache()
                {
                    id = (int)reader["id"],
                    IdPersonne = (int)reader["IdPersonne"],
                    IdCategorie = (int)reader["IdCategorie"],
                    nom = (string)reader["nom"],
                    descript = (string)reader["descript"],
                    dateDeCreation = (DateTime)reader["dateDeCreation"],
                    datePredicFin = (DateTime)reader["datePredicFin"],
                    dateFinReelle = reader["dateFinReelle"] != DBNull.Value ? (DateTime)reader["dateFinReelle"]: null
                };
            }
        }

        public IEnumerable<Tache> GetByCat(int IdCategorie)
        {
            List<Tache> tacheByCat = new List<Tache>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM tache WHERE IdCategorie = @id";
                command.Parameters.AddWithValue("id", IdCategorie);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Tache getTachesPerson = new Tache()
                    {
                        id = (int)reader["id"],
                        IdPersonne = (int)reader["IdPersonne"],
                        IdCategorie = (int)reader["IdCategorie"],
                        nom = (string)reader["nom"],
                        descript = (string)reader["descript"],
                        dateDeCreation = (DateTime)reader["dateDeCreation"],
                        datePredicFin = (DateTime)reader["datePredicFin"],
                        dateFinReelle = reader["dateFinReelle"] as DateTime? ?? null
                    };
                    tacheByCat.Add(getTachesPerson);
                }
                connection.Close();
            }
            return tacheByCat;
        }

        public IEnumerable<Tache> GetByPerson(int IdPersonne)
        {
            List<Tache> tacheAChercher = new List<Tache>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM tache WHERE IdPersonne = @id";
                command.Parameters.AddWithValue("id", IdPersonne);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Tache getTachesPerson = new Tache()
                    {
                        id = (int)reader["id"],
                        IdPersonne = (int)reader["IdPersonne"],
                        IdCategorie = (int)reader["IdCategorie"],
                        nom = (string)reader["nom"],
                        descript = (string)reader["descript"],
                        dateDeCreation = (DateTime)reader["dateDeCreation"],
                        datePredicFin = (DateTime)reader["datePredicFin"],
                        dateFinReelle = reader["dateFinReelle"] as DateTime? ?? null
                    };
                    tacheAChercher.Add(getTachesPerson);
                }
                connection.Close();
            }
            return tacheAChercher;
        }

        public IEnumerable<Tache> GetByNotFinish()
        {
            List<Tache> tacheNotFinish = new List<Tache>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM tache WHERE dateFinReelle is null";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Tache getTachesNotFinish = new Tache()
                    {
                        id = (int)reader["id"],
                        IdPersonne = (int)reader["IdPersonne"],
                        IdCategorie = (int)reader["IdCategorie"],
                        nom = (string)reader["nom"],
                        descript = (string)reader["descript"],
                        dateDeCreation = (DateTime)reader["dateDeCreation"],
                        datePredicFin = (DateTime)reader["datePredicFin"],
                        dateFinReelle = reader["dateFinReelle"] as DateTime? ?? null
                    };
                    tacheNotFinish.Add(getTachesNotFinish);
                }
                connection.Close();
            }
            return tacheNotFinish;
        }

        public int AddTache(Tache tacheAdd)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"INSERT INTO tache (id, IdPersonne, IdCategorie, nom, descript, dateDeCreation, datePredicFin) VALUES (@id, @idPersonne, @idCategorie, @nom, @descript, @dateDeCreation, @datePredicFin)";

                command.Parameters.AddWithValue("id", tacheAdd.id);
                command.Parameters.AddWithValue("idPersonne", tacheAdd.IdPersonne);
                command.Parameters.AddWithValue("idCategorie", tacheAdd.IdCategorie);
                command.Parameters.AddWithValue("nom", tacheAdd.nom);
                command.Parameters.AddWithValue("descript", tacheAdd.descript);
                command.Parameters.AddWithValue("dateDeCreation", tacheAdd.dateDeCreation);
                command.Parameters.AddWithValue("datePredicFin", tacheAdd.datePredicFin);
                return command.ExecuteNonQuery();
            }
        }

        public void ModifyTache(Tache tacheToModif)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "UPDATE tache SET IdPersonne = @idPersonne, IdCategorie = @idCategorie, nom = @nom, descript = @descript, dateDeCreation = @dateDeCreation, datePredicFin = @datePredicFin WHERE id = @id";
                command.Parameters.AddWithValue("id", tacheToModif.id);
                command.Parameters.AddWithValue("idPersonne", tacheToModif.IdPersonne);
                command.Parameters.AddWithValue("idCategorie", tacheToModif.IdCategorie);
                command.Parameters.AddWithValue("nom", tacheToModif.nom);
                command.Parameters.AddWithValue("descript", tacheToModif.descript);
                command.Parameters.AddWithValue("dateDeCreation", tacheToModif.dateDeCreation);
                command.Parameters.AddWithValue("datePredicFin", tacheToModif.datePredicFin);

                command.ExecuteNonQuery();
            }
        }

        public int DeleteTache(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DELETE FROM tache WHERE id = @id";
                command.Parameters.AddWithValue("id", id);
                return command.ExecuteNonQuery();
            }
        }

        public void EndTache(Tache endTache)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "UPDATE tache SET dateFinReelle = @dateFinReelle WHERE id = @id";
                command.Parameters.AddWithValue("id", endTache.id);
                command.Parameters.AddWithValue("dateFinReelle", endTache.dateFinReelle);

                command.ExecuteNonQuery();
            }
        }
    }
}
