﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class PersonneService
    {
        public string ConnectionStringSSMS
        {
            get { return "server=MYA-PC\\SQLEXPRESS;database=Ex01;integrated security=true"; }
        }

        public IEnumerable<Personne> GetAll()
        {
            List<Personne> person = new List<Personne>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM personne";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Personne personToSee = new Personne()
                    {
                        IdPersonne = (int)reader["IdPersonne"],
                        nom = (string)reader["nom"],
                        prenom = (string)reader["prenom"]
                    };
                    person.Add(personToSee);
                }
                connection.Close();
            }
            return person;
        }

        public Personne GetById(int IdPersonne)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM personne WHERE IdPersonne = @id";
                command.Parameters.AddWithValue("id", IdPersonne);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return new Personne()
                {
                    IdPersonne = (int)reader["IdPersonne"],
                    nom = (string)reader["nom"],
                    prenom = (string)reader["prenom"]
                };
            }
        }

        public int AddPersonne(Personne personneAdd)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"INSERT INTO personne (IdPersonne, nom, prenom) VALUES (@id, @nom, @prenom)";
                command.Parameters.AddWithValue("id", personneAdd.IdPersonne);
                command.Parameters.AddWithValue("nom", personneAdd.nom);
                command.Parameters.AddWithValue("prenom", personneAdd.prenom);
                return command.ExecuteNonQuery();
            }
        }

        public void ModifyPerson(Personne personToModif)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "UPDATE personne SET IdPersonne = @id, nom = @nom, prenom = @prenom WHERE IdPersonne = @id";
                command.Parameters.AddWithValue("id", personToModif.IdPersonne);
                command.Parameters.AddWithValue("nom", personToModif.nom);
                command.Parameters.AddWithValue("prenom", personToModif.prenom);

                command.ExecuteNonQuery();

            }
        }

        public int DeletePersonne(int IdPersonne)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DELETE FROM personne WHERE IdPersonne = @id";
                command.Parameters.AddWithValue("id", IdPersonne);
                return command.ExecuteNonQuery();
            }
        }
    }
}
