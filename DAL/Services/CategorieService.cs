﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class CategorieService
    {
        public string ConnectionStringSSMS
        {
            get { return "server=MYA-PC\\SQLEXPRESS;database=Ex01;integrated security=true"; }
        }

        public IEnumerable<Categorie> GetAll()
        {
            List<Categorie> cat = new List<Categorie>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM categorie";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Categorie catToSee = new Categorie()
                    {
                        IdCategorie = (int)reader["IdCategorie"],
                        nom = (string)reader["nom"]
                    };
                    cat.Add(catToSee);
                }
                connection.Close();
            }
            return cat;
        }

        public Categorie GetById(int IdCategorie)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM categorie WHERE IdCategorie = @id";
                command.Parameters.AddWithValue("id", IdCategorie);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return new Categorie()
                {
                    IdCategorie = (int)reader["IdCategorie"],
                    nom = (string)reader["nom"]
                };
            }
        }

        public int AddCategorie(Categorie categorieAdd)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"INSERT INTO categorie (IdCategorie, nom) VALUES (@id, @nom)";
                command.Parameters.AddWithValue("id", categorieAdd.IdCategorie);
                command.Parameters.AddWithValue("nom", categorieAdd.nom);
                return command.ExecuteNonQuery();
            }
        }

        public int DeleteCategorie(int IdCategorie)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringSSMS))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DELETE FROM categorie WHERE IdCategorie = @id";
                command.Parameters.AddWithValue("id", IdCategorie);
                return command.ExecuteNonQuery();
            }
        }
    }
}
