﻿using DAL.Services;
using DAL.Entities;
using System.Collections.Generic;
using System;

namespace Consommation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CategorieService catServ = new CategorieService();
            PersonneService personneServ = new PersonneService();
            TacheService tacheServ = new TacheService();

            ////Récupérer toutes les catégories
            //IEnumerable<Categorie> lescat = catServ.GetAll();
            //foreach (Categorie cat in lescat)
            //{
            //    Console.WriteLine($"Id : {cat.IdCategorie} - Nom : {cat.nom}");
            //}
            ////Récupérer une catégorie via son ID
            //Categorie Bricoler = catServ.GetById(5);
            //Console.WriteLine($"{Bricoler.IdCategorie} - {Bricoler.nom}");

            ////Ajouter une categorie
            //Categorie catAAjouter = new Categorie()
            //{
            //    IdCategorie = 7,
            //    nom = "Coder en ADO"
            //};
            ////catServ.AddCategorie(catAAjouter);

            ////Supprimer une catégorie
            ////catServ.DeleteCategorie(0);

            ////Récupérer toute la liste de personnes
            //IEnumerable<Personne> lesPersonnes = personneServ.GetAll();
            //foreach (Personne person in lesPersonnes)
            //{
            //    Console.WriteLine($"Id :{person.IdPersonne} - Nom : {person.nom} - Prénom : {person.prenom}");
            //}

            ////Récupérer une personne via son Id
            //Personne Quentin = personneServ.GetById(1);
            //Console.WriteLine($"{Quentin.IdPersonne} - {Quentin.nom} - {Quentin.prenom}");

            ////Ajouter une personne
            //Personne personneAAjouter = new Personne()
            //{
            //    IdPersonne = 8,
            //    nom = "Gelpi",
            //    prenom = "Yûna"
            //};
            ////personneServ.AddPersonne(personneAAjouter);

            ////Supprimer une personne
            ////personneServ.DeletePersonne(8);

            ////Modifier une personne

            //Quentin.nom = "Duku";
            //personneServ.ModifyPerson(Quentin);

            ////Afficher toutes les tâches
            //IEnumerable<Tache> lesTaches = tacheServ.GetAll();
            //foreach (Tache tache in lesTaches)
            //{
            //    Console.WriteLine($"Id :{tache.id} - IdPersonne : {tache.IdPersonne} - IdCategorie : {tache.IdCategorie} - Nom : {tache.nom} - Description : {tache.descript} - Date de création : {tache.dateDeCreation} - Date de fin : {tache.datePredicFin} - Date de fin réelle : {tache.dateFinReelle}");
            //}

            ////Afficher une tâche par Id
            Tache Idk = tacheServ.GetById(4);
            Console.WriteLine($"{Idk.id} - {Idk.IdCategorie} - {Idk.IdPersonne} - {Idk.nom} - {Idk.descript} - {Idk.dateDeCreation} - {Idk.datePredicFin} - {Idk.datePredicFin}");

            ////Récupérer toutes les tâches d'une catégorie
            IEnumerable<Tache> lesTachesPC = tacheServ.GetByCat(4);
            foreach (Tache tache in lesTachesPC)
            {
                Console.WriteLine($"Id :{tache.id} - IdPersonne : {tache.IdPersonne} - IdCategorie : {tache.IdCategorie} - Nom : {tache.nom} - Description : {tache.descript} - Date de création : {tache.dateDeCreation} - Date de fin : {tache.datePredicFin} - Date de fin réelle : {tache.dateFinReelle}");
            }

            //Récupérer toutes les tâches attribuées à une personne
            IEnumerable<Tache> lesTachesPP = tacheServ.GetByPerson(1);
            foreach (Tache tache in lesTachesPP)
            {
                Console.WriteLine($"Id :{tache.id} - IdPersonne : {tache.IdPersonne} - IdCategorie : {tache.IdCategorie} - Nom : {tache.nom} - Description : {tache.descript} - Date de création : {tache.dateDeCreation} - Date de fin : {tache.datePredicFin} - Date de fin réelle : {tache.dateFinReelle}");
            }
            //Récupérer toutes les tâches qui ne sont pas terminées
            IEnumerable<Tache> lesTachesNF = tacheServ.GetByNotFinish();
            foreach (Tache tache in lesTachesNF)
            {
                Console.WriteLine($"Tâche non terminée : Id :{tache.id} - IdPersonne : {tache.IdPersonne} - IdCategorie : {tache.IdCategorie} - Nom : {tache.nom} - Description : {tache.descript} - Date de création : {tache.dateDeCreation} - Date de fin : {tache.datePredicFin} - Date de fin réelle : {tache.dateFinReelle}");
            }

            //Ajouter une tâche
            Tache tacheAAjouter = new Tache()
            {
                id = 10,
                IdCategorie = 2,
                IdPersonne = 1,
                nom = "Test",
                descript = "Description tache",
                dateDeCreation = DateTime.Now,
                datePredicFin = new DateTime(2022, 7, 1)
            };
            //tacheServ.AddTache(tacheAAjouter);


            //Modifier une tâche
            Idk.descript = "Modification test blabla";
            tacheServ.ModifyTache(Idk);

            //Supprimer une tâche
            tacheServ.DeleteTache(7);

            //Finir une tâche
            Idk.dateFinReelle = DateTime.Now;
            tacheServ.EndTache(Idk);

            Console.ReadLine();
        }
    }
}
